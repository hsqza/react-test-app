import React from 'react';
import './App.css';

import GuessedWord from './components/GuessedWord';
import Congrats from './components/Congrats';

function App() {
  return (
    <div className="container">
      <h1>Jotto</h1>
      <Congrats success={false}/>
      <GuessedWord guessedWords={[
        { guessedWord: 'train', letterMatchCount: 3 }
      ]}/>
    </div>
  );
}

export default App;
