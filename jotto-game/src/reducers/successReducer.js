import * as ACTION from '../actions/types';

export default ( state=false, action ) => {
  switch (action.type) {
    case ACTION.CORRECT_GUESS:
      return true;
    default:
      return state;
  }
}