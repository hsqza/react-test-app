import * as ACTION from '../actions/types';
import successReducer from './successReducer';

test('return default state of "false" when no action passed', () => {
  const newState = successReducer(undefined, {});
  expect(newState).toBe(false);
});

test('return state of true upon receiving an action of type "CORRECT_GUESS"', () => {
  const newState = successReducer(undefined, { type: ACTION.CORRECT_GUESS });
  expect(newState).toBe(true);
});